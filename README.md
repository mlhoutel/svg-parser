# svg-parser

Playing with svg in a `js` script.

## Current input

![input](input/world.svg)

## Current output

![output](output/world.svg)