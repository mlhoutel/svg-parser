#!/usr/bin/node
const INPUT_PATH = "input_path";
const OUTPUT_PATH = "output_path";

main();

async function main() {
    try {
        const inputs = handleUserInputs();

        const datas = await readFile(inputs[INPUT_PATH]);

        const parsed = parseInputXML(datas);

        countryGradient(parsed);
        // editFrance(parsed);

        const serialized = serializeXML(parsed);

        writeFile(inputs[OUTPUT_PATH], serialized);
    } catch (err) {
        console.error(err.message);
    }
}


function handleUserInputs() {
    const args = process.argv.slice(2);

    const fields = [INPUT_PATH, OUTPUT_PATH];
    const inputs = fields.reduce((acc, label, index) => ({ ...acc, [label]: args[index] }), {});
    const missing = fields.find((label) => inputs[label] === undefined);

    if (missing) {
        throw new Error(`The '${missing}' parameter is missing. Please try again with it.`);
    }

    return inputs;
}

function readFile(location) {
    const fs = require('fs');

    return new Promise((resolve, reject) => {
        fs.readFile(location, 'utf8', (error, data) => {
            error ? reject(new Error(`An error occured while trying to read the ${location} file (${error}).`)) : resolve(data);
        });
    });
}

function parseInputXML(source) {
    const { DOMParser } = require('@xmldom/xmldom');

    const parsed = new DOMParser().parseFromString(source, 'text/xml');

    return parsed;
}

function serializeXML(source) {
    const { XMLSerializer } = require('@xmldom/xmldom');

    const serialized = new XMLSerializer().serializeToString(source);

    return serialized;
}

function writeFile(location, data) {
    const fs = require('fs');

    return new Promise((resolve, reject) => {
        fs.writeFile(location, data, (error) => {
            error ? reject(new Error(`An error occured while trying to write in ${location} (${error}).`)) : resolve();
        });
    });
}

function countryGradient(XML) {
    const { parseSVG, makeAbsolute } = require('svg-path-parser');

    const paths = XML.getElementsByTagName("path");
    const svg = XML.getElementsByTagName("svg")[0];

    for (let i = 0; i < paths.length; i++) {
        let path = paths[i];

        // if (!path.getAttribute("class")) continue; // only alter countries

        const points = parseSVG(path.getAttribute("d"));

        makeAbsolute(points);

        const sums = points.reduce((a, b) => ({ x: a.x + b.x, y: a.y + b.y }), { x: 0, y: 0 });

        const centroid = [sums.x / points.length, sums.y / points.length];
        const max_size = [svg.getAttribute("width"), svg.getAttribute("height")];

        const norm_x = centroid[0] / max_size[0];
        const norm_y = centroid[1] / max_size[1];

        // let norm = norm_x > norm_y ? norm_x : norm_y;
        let norm = norm_x * 0.5 + norm_y * 0.5;
        // let norm = norm_x;
        // let norm = norm_y;

        const color = getGradientColor('#EF3D59', '#4AB19D', norm);

        const circle = XML.createElement("circle");
        circle.setAttribute("cx", centroid[0]);
        circle.setAttribute("cy", centroid[1]);
        circle.setAttribute("r", 7);
        circle.setAttribute("stroke-width", "1");
        circle.setAttribute("style", `fill: ${color}`);

        // console.log(circle);
        svg.appendChild(circle);

        path.setAttribute('style', `fill: ${color}`);
    }
}

function editFrance(XML) {

    const France = XML.getElementsByClassName("France")[1];
    France.setAttribute('style', 'fill: red');
    France.setAttribute('transform', 'translate(0 -10)');

    // parsed.removeChild(parsed.getElementsByClassName("France")[1]);
    console.log(XML.getElementsByClassName("France").length);
}

function getGradientColor(start_color, end_color, percent) {
    // strip the leading # if it's there
    start_color = start_color.replace(/^\s*#|\s*$/g, '');
    end_color = end_color.replace(/^\s*#|\s*$/g, '');

    // convert 3 char codes --> 6, e.g. `E0F` --> `EE00FF`
    if (start_color.length == 3) {
        start_color = start_color.replace(/(.)/g, '$1$1');
    }

    if (end_color.length == 3) {
        end_color = end_color.replace(/(.)/g, '$1$1');
    }

    // get colors
    var start_red = parseInt(start_color.substr(0, 2), 16),
        start_green = parseInt(start_color.substr(2, 2), 16),
        start_blue = parseInt(start_color.substr(4, 2), 16);

    var end_red = parseInt(end_color.substr(0, 2), 16),
        end_green = parseInt(end_color.substr(2, 2), 16),
        end_blue = parseInt(end_color.substr(4, 2), 16);

    // calculate new color
    var diff_red = end_red - start_red;
    var diff_green = end_green - start_green;
    var diff_blue = end_blue - start_blue;

    diff_red = ((diff_red * percent) + start_red).toString(16).split('.')[0];
    diff_green = ((diff_green * percent) + start_green).toString(16).split('.')[0];
    diff_blue = ((diff_blue * percent) + start_blue).toString(16).split('.')[0];

    // ensure 2 digits by color
    if (diff_red.length === 1) diff_red = '0' + diff_red;
    if (diff_green.length === 1) diff_green = '0' + diff_green;
    if (diff_blue.length === 1) diff_blue = '0' + diff_blue;

    return '#' + diff_red + diff_green + diff_blue;
};